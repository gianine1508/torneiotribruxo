import "./App.css";
import React from "react";
import Card from "./componentes/card/Card";
import "./index.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      personagens: [],
      personagensEscolhidos: [],
      show: false,
      showInfor: true,
    };
  }

  getRandom = (max) => {
    return Math.floor(Math.random() * max + 1);
  };

  componentDidMount = () => {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => {
        this.setState({ personagens: response });
        this.escolhaDeBruxo();
      })
      .catch((error) => console.log(error));
  };

  escolhaDeBruxo = () => {
    let { personagens } = this.state;
    let contador = 0;
    let array = [];
    let person = [];

    do {
      let NumAle = Math.floor(Math.random() * 11);

      if (!array.includes(NumAle)) {
        array.push(NumAle);
        contador = contador + 1;
      }
    } while (contador < 3);

    for (let i = 0; i < 3; i++) {
      person.push(personagens[array[i]]);
    }

    this.setState({
      personagensEscolhidos: person,
    });
  };

  showEscolhidos = () => {
    this.setState({ show: true });
    this.setState({ showInfor: false });
  };

  render() {
    const { personagensEscolhidos, show, showInfor } = this.state;
    return (
      <div className="App">
        {showInfor && (
          <div>
            <h1>Bem-Vindo ao Torneio Tribruxo</h1>
            <h3>
              <cite>
                “O Torneio Tribruxo foi criado há uns setecentos anos, como uma
                competição amistosa entre as três maiores escolas européias de
                Bruxaria ― Hogwarts, Beauxbatons e Durmstrang. Um campeão foi
                eleito para representar cada escola e os três campeões
                competiram em três tarefas mágicas. As escolas se revezaram para
                sediar o torneio a cada cinco anos, e todos rdaram que era uma
                excelente maneira de estabelecer laços entre os jovens bruxos e
                bruxas de diferentes nacionalidades ― até que a taxa de
                mortalidade se tornou tão alta que o torneio foi interrompido.
                (…) Durante séculos houve várias tentativas de reiniciar o
                torneio, nenhuma das quais foi bem-sucedida. No entanto, os
                nossos Departamentos de Cooperação Internacional em Magia e de
                Jogos e Esportes Mágicos decidiram que já era hora de fazer uma
                nova tentativa. Trabalhamos muito durante o verão para garantir
                que, desta vez, nenhum campeão seja exposto a um perigo mortal.”
              </cite>
              <p>
                <b>
                  <cite>
                    So que esse ano sera diferente, sera realizdo um pre-torneio
                    em Hogwarts escolhendo um aluno que participaram do torneio
                    sendo nosso representante.
                  </cite>
                </b>
              </p>
            </h3>
            <h2>
              <b>
                Este o calice de fogo que escolherá 3 participantes que faram
                parte do torneio.
              </b>
              <p>
                <i>
                  <b>
                    O ganhador do torneio recebera a taça mais 1000 Galeões e a
                    'Glória Eterna'
                  </b>
                </i>
                Click no calice e veja os participantes
              </p>
            </h2>
            <input
              type="button"
              onClick={this.showEscolhidos}
              className="botao"
            />
          </div>
        )}
        {show && <Card estudantes={personagensEscolhidos} />}
      </div>
    );
  }
}

export default App;
