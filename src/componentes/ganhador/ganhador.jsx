import React from "react";
import "./style.css";

class Ganhador extends React.Component {
  render() {
    return (
      <>
        <h1>O ganhador do Tornio Tribruxo</h1>
        <div className="winner">
          <h3>{this.props.win.house}</h3>
          <img src={this.props.win.image} alt={this.props.win.name}></img>
          <p>{this.props.win.name}</p>
        </div>
      </>
    );
  }
}

export default Ganhador;
