import React from "react";
import "./style.css";

class Info extends React.Component {
  render() {
    return (
      <>
        {this.props.estudantes.map(({ house, name, image }, index) => (
          <div key={index}>
            <h3>{house}</h3>
            <img src={image} alt={name}></img>
            <p>{name}</p>
          </div>
        ))}
      </>
    );
  }
}

export default Info;
