import React from "react";
import "./style.css";
import Infor from "../info/Infor";
import Ganhador from "../ganhador/ganhador";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showBruxos: true,
      ganhador: "",
      showGanhador: false,
    };
  }

  ganhadorTorneio = () => {
    let NumAle = Math.floor(Math.random() * 3);
    this.setState({ ganhador: this.props.estudantes[NumAle] });
    this.setState({ showGanhador: true });
  };

  showEsconderBruxos = () => {
    const { showBruxos } = this.state;
    this.setState({ showBruxos: !showBruxos });
    this.ganhadorTorneio();
  };

  newGame = () => {
    window.location.reload();
  };

  render() {
    const { showBruxos, showGanhador } = this.state;
    return (
      <>
        {showBruxos && (
          <div>
            <h1>Participantes do Torneio</h1>
            <div className="card">
              <Infor estudantes={this.props.estudantes}></Infor>
            </div>

            <input
              type="button"
              onClick={this.showEsconderBruxos}
              className="botao"
            />
            <h4>Clice na taça e veja quem e o ganhador</h4>
          </div>
        )}

        {showGanhador && (
          <div>
            <Ganhador className="card" win={this.state.ganhador}></Ganhador>
            <button onClick={this.newGame}>Novo Torneio</button>
          </div>
        )}
      </>
    );
  }
}

export default Card;
